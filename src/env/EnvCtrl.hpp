#pragma once

#include <stdint.h>
#include <radio/UplinkCb.hpp>
#include <avr/pgmspace.h>
#include "EnvRuntimeCfg.hpp"

namespace env {

class EnvCtrl
{
public:
    EnvCtrl();
    ~EnvCtrl();

    EnvCtrl(EnvCtrl&) = delete;
    EnvCtrl& operator=(EnvCtrl&) = delete;
    EnvCtrl(EnvCtrl&&) = delete;
    EnvCtrl& operator=(EnvCtrl&&) = delete;

    const EnvRuntimeCfg* getRuntimeCfg() const;
    void setTxIntervalSec(const uint32_t txIntervalSec);
    radio::UplinkCb bindToCallback();

private:
    static void onDataReceived(const uint8_t* const data, const uint8_t dataLen, void* calleeContext);

    EnvRuntimeCfg runtimeCfg;

    static constexpr const char PROGMEM SET_INTERVAL_REQ[] = "INTERVAL=";
};

} // namespace env
