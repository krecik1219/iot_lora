#include "EnvCtrl.hpp"
#include "EnvConfiguration.hpp"
#include <string.h>
#include <stdlib.h>

namespace env {

constexpr const char PROGMEM EnvCtrl::SET_INTERVAL_REQ[];

EnvCtrl::EnvCtrl()
    : runtimeCfg{INITIAL_TX_INTERVAL_SEC}
{
}

EnvCtrl::~EnvCtrl() = default;

const EnvRuntimeCfg* EnvCtrl::getRuntimeCfg() const
{
    return &runtimeCfg;
}

void EnvCtrl::setTxIntervalSec(const uint32_t txIntervalSec)
{
    this->runtimeCfg.txIntervalSec = txIntervalSec;
}

radio::UplinkCb EnvCtrl::bindToCallback()
{
    return {this, &EnvCtrl::onDataReceived};
}

void EnvCtrl::onDataReceived(const uint8_t* const data, const uint8_t dataLen, void* calleeContext)
{
    // assuming Little-Endian payload
    constexpr uint8_t intervalSetReqLen = sizeof(EnvCtrl::SET_INTERVAL_REQ) - 1;
    if(dataLen <= intervalSetReqLen)
        return;

    if(memcmp_P(data, EnvCtrl::SET_INTERVAL_REQ, intervalSetReqLen) != 0)
    {
        Serial.println(F("Rec not eq"));
        return;
    }

    const uint8_t effectiveReqSize = dataLen - intervalSetReqLen;
    constexpr uint8_t maxPayloadBytes = 4;
    uint8_t bytesRead = 0u;
    uint8_t buffer[maxPayloadBytes]{};
    for(auto i = intervalSetReqLen; i < dataLen && bytesRead < maxPayloadBytes; i ++)
    {
        buffer[bytesRead] = data[i];
        bytesRead++;
    }
    const uint32_t interval = strtoul(reinterpret_cast<const char*>(buffer), nullptr, 10);
    Serial.print(F("IntervalToSet="));
    Serial.println(interval);
    auto* self = static_cast<EnvCtrl*>(calleeContext);
    self->setTxIntervalSec(interval);
}

} // namespace env
