#pragma once

#include <Arduino.h>
#include <lmic.h>
#include <hal.h>

namespace env {

inline constexpr u1_t PROGMEM APPEUI[8] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
inline constexpr u1_t PROGMEM DEVEUI[8] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
inline constexpr u1_t PROGMEM APPKEY[16] = {
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
};
inline constexpr lmic_pinmap PINS = {10, LMIC_UNUSED_PIN,LMIC_UNUSED_PIN, {2, 3, LMIC_UNUSED_PIN}};

inline constexpr uint8_t SENSORS_NUM = 2;
inline constexpr uint8_t TEMP_SENSOR_ANALOG_PIN = 0;
inline constexpr uint8_t LIGHT_SENSOR_ANALOG_PIN = 2;
inline constexpr uint8_t INITIAL_TX_INTERVAL_SEC = 30;

} // namespace env

