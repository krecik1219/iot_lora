#pragma once

#include <stdint.h>

namespace env {

struct EnvRuntimeCfg
{
    uint32_t txIntervalSec;
};

} // namespace env
