#pragma once

#include <Arduino.h>
#include <lmic.h>
#include <hal.h>

namespace env {

inline constexpr u1_t PROGMEM APPEUI[8] = { 0x8D, 0x91, 0x03, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };
inline constexpr u1_t PROGMEM DEVEUI[8] = { 0x0F, 0x82, 0x5E, 0xB6, 0xF6, 0x7F, 0x41, 0x00 };
inline constexpr u1_t PROGMEM APPKEY[16] = {
    0x2B, 0xDD, 0x36, 0x1D, 0x39, 0x25, 0x20, 0x9B, 0x17, 0x72, 0xD2, 0xC2, 0xFF, 0x73, 0x1D, 0xE7
};
inline constexpr lmic_pinmap PINS = {10, LMIC_UNUSED_PIN,LMIC_UNUSED_PIN, {2, 3, LMIC_UNUSED_PIN}};

inline constexpr uint8_t SENSORS_NUM = 2;
inline constexpr uint8_t TEMP_SENSOR_ANALOG_PIN = 0;
inline constexpr uint8_t LIGHT_SENSOR_ANALOG_PIN = 2;
inline constexpr uint8_t INITIAL_TX_INTERVAL_SEC = 30;

} // namespace env

