#include <Arduino.h>
#include <env/EnvConfiguration.hpp>
#include <measurement/MeasurementCollector.hpp>
#include <radio/LoraCfg.hpp>
#include <radio/LoraCtrl.hpp>
#include <measurement/TempSensor.hpp>
#include <measurement/LightSensor.hpp>
#include <env/EnvCtrl.hpp>

void prepare()
{
    Serial.begin(115200);
    Serial.println(F("Starting"));
}

int main()
{
    init();  // MANDATORY!

    prepare();

    env::EnvCtrl envCtrl{};

    radio::LoraCfg cfg{};
    cfg.appEui = env::APPEUI;
    cfg.appKey = env::APPKEY;
    cfg.devEui = env::DEVEUI;
    cfg.pinsCfg = env::PINS;

    radio::LoraCtrl::initLoraCtrl(cfg, envCtrl.getRuntimeCfg());

    measurement::TempSensor tempSensor{env::TEMP_SENSOR_ANALOG_PIN};
    measurement::LightSensor lightSensor{env::LIGHT_SENSOR_ANALOG_PIN};
    measurement::MeasurementCollector measurementCollector{};
    measurementCollector.addSensor(&tempSensor);
    measurementCollector.addSensor(&lightSensor);
    auto& loraCtrl = radio::LoraCtrl::getInstance();

    loraCtrl.registerPayloadSetCb(measurementCollector.bindToCallback());
    loraCtrl.registerUplinkCb(envCtrl.bindToCallback());
    loraCtrl.beginSending();

    while(true)
    {
        loraCtrl.proceedScheduler();
    }
}
