#pragma once

#include <stdint.h>

namespace radio {

struct UplinkCb
{
    using callbackFunction = void (*) (const uint8_t* const data, const uint8_t dataLen, void* calleeContext);
    void* cbContext;
    callbackFunction cb;
};

} // namespace radio
