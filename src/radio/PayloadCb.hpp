#pragma once

#include <stdint.h>

namespace radio {

struct PayloadCb
{
    using callbackFunction = void (*) (uint8_t* payload, void* calleeContext);
    void* cbContext;
    callbackFunction cb;
};

} // namespace radio
