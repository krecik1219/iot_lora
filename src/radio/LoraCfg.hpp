#pragma once

#include <Arduino.h>
#include <lmic.h>
#include <hal.h>

namespace radio {

struct LoraCfg
{
    const u1_t* PROGMEM appEui;
    const u1_t* PROGMEM devEui;
    const u1_t* PROGMEM appKey;
    struct lmic_pinmap pinsCfg;
};


} // namespace radio
