#include "LoraCtrl.hpp"
#include <Arduino.h>
#include <env/EnvConfiguration.hpp>
#include <env/EnvRuntimeCfg.hpp>
#include <lmic.h>
#include <hal.h>

// necessary global structs for lmic
const lmic_pinmap lmic_pins = env::PINS;

// necessary functions for lmic
void os_getArtEui(u1_t* buf)
{
    auto& loraCtrl = radio::LoraCtrl::getInstance();
    memcpy_P(buf, loraCtrl.getCfg().appEui, sizeof(env::APPEUI));
    Serial.print(F("os_getArtEui: "));
    for(auto i = 0u; i < 8; i++)
        Serial.print(buf[i]);
    Serial.println();
}

void os_getDevEui(u1_t* buf)
{
    auto& loraCtrl = radio::LoraCtrl::getInstance();
    memcpy_P(buf, loraCtrl.getCfg().devEui, sizeof(env::DEVEUI));
    Serial.print(F("os_getDevEui: "));
    for(auto i = 0u; i < 8; i++)
        Serial.print(buf[i]);
    Serial.println();
}

void os_getDevKey(u1_t* buf)
{
    auto& loraCtrl = radio::LoraCtrl::getInstance();
    memcpy_P(buf, loraCtrl.getCfg().appKey, sizeof(env::APPKEY));
    Serial.print(F("os_getDevKey: "));
    for(auto i = 0u; i < 16; i++)
        Serial.print(buf[i]);
    Serial.println();
}

void do_send(osjob_t* job)
{
    auto& loraCtrl = radio::LoraCtrl::getInstance();
    loraCtrl.payloadSet();
    auto* payload = loraCtrl.getPayload();
    Serial.print(F("payload="));
    Serial.println(reinterpret_cast<const char*>(payload));
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND)
    {
        Serial.println(F("OP_TXRXPEND, not sending"));
    }
    else
    {
        // Prepare upstream data transmission at the next possible time.
        //LMIC_setTxData2(1, payloadWrapper.payload, payloadWrapper.length, 0);
        LMIC_setTxData2(1, payload, 16, 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    auto& loraCtrl = radio::LoraCtrl::getInstance();
    switch(ev) {
    case EV_SCAN_TIMEOUT:
        Serial.println(F("EV_SCAN_TIMEOUT"));
        break;
    case EV_BEACON_FOUND:
        Serial.println(F("EV_BEACON_FOUND"));
        break;
    case EV_BEACON_MISSED:
        Serial.println(F("EV_BEACON_MISSED"));
        break;
    case EV_BEACON_TRACKED:
        Serial.println(F("EV_BEACON_TRACKED"));
        break;
    case EV_JOINING:
        Serial.println(F("EV_JOINING"));
        break;
    case EV_JOINED:
        Serial.println(F("EV_JOINED"));

        // Disable link check validation (automatically enabled
        // during join, but not supported by TTN at this time).
        LMIC_setLinkCheckMode(0);
        break;
    case EV_RFU1:
        Serial.println(F("EV_RFU1"));
        break;
    case EV_JOIN_FAILED:
        Serial.println(F("EV_JOIN_FAILED"));
        break;
    case EV_REJOIN_FAILED:
        Serial.println(F("EV_REJOIN_FAILED"));
        break;
        break;
    case EV_TXCOMPLETE:
        Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
        if (LMIC.txrxFlags & TXRX_ACK)
            Serial.println(F("Received ack"));
        if (LMIC.dataLen) {
            Serial.print(F("Received "));
            Serial.print(LMIC.dataLen);
            Serial.println(F(" bytes of payload"));
            loraCtrl.onUplinkReceived(LMIC.frame + LMIC.dataBeg, LMIC.dataLen);
        }
        // Schedule next transmission
        Serial.print(F("Schedule next: "));
        Serial.println(loraCtrl.getTxInterval());
        os_setTimedCallback(loraCtrl.getSendJobPtr(), os_getTime() + sec2osticks(loraCtrl.getTxInterval()), do_send);
        break;
    case EV_LOST_TSYNC:
        Serial.println(F("EV_LOST_TSYNC"));
        break;
    case EV_RESET:
        Serial.println(F("EV_RESET"));
        break;
    case EV_RXCOMPLETE:
        // data received in ping slot
        Serial.println(F("EV_RXCOMPLETE"));
        break;
    case EV_LINK_DEAD:
        Serial.println(F("EV_LINK_DEAD"));
        break;
    case EV_LINK_ALIVE:
        Serial.println(F("EV_LINK_ALIVE"));
        break;
    default:
        Serial.println(F("Unknown event"));
        break;
    }
}

namespace radio {

// LoraCtrl implementation
LoraCtrl LoraCtrl::instance{};

LoraCtrl::LoraCtrl()
    : cfg{}
    , runtimeCfg{nullptr}
    , payload{}
    , sendjob{}
    , payloadCb{}
    , uplinkCb{}
{
}

LoraCtrl::~LoraCtrl() = default;

LoraCtrl& LoraCtrl::initLoraCtrl(const LoraCfg& loraCfg, const env::EnvRuntimeCfg* const runtimeCfg)
{
    Serial.println(F("initLoraCtrl"));
    auto& loraCtrl = getInstance();
    loraCtrl.cfg = loraCfg;
    loraCtrl.runtimeCfg = runtimeCfg;

    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();

    return instance;
}

LoraCtrl& LoraCtrl::getInstance()
{
    return instance;
}

const LoraCfg& LoraCtrl::getCfg() const
{
    return cfg;
}

osjob_t* LoraCtrl::getSendJobPtr()
{
    return &sendjob;
}

uint8_t* LoraCtrl::getPayload()
{
    return payload;
}

void LoraCtrl::beginSending()
{
    do_send(&sendjob);
}

void LoraCtrl::proceedScheduler()
{
    os_runloop_once();
}

void LoraCtrl::payloadSet()
{
    memset(payload, 0, payloadSize);
    payloadCb.cb(payload, payloadCb.cbContext);
}

void LoraCtrl::registerPayloadSetCb(PayloadCb payloadCb)
{
    this->payloadCb = payloadCb;
}

void LoraCtrl::onUplinkReceived(const uint8_t* const data, const uint8_t dataLen)
{
    uplinkCb.cb(data, dataLen, uplinkCb.cbContext);
}

void LoraCtrl::registerUplinkCb(UplinkCb uplinkCb)
{
    this->uplinkCb = uplinkCb;
}

uint32_t LoraCtrl::getTxInterval() const
{
    return runtimeCfg != nullptr? runtimeCfg->txIntervalSec : env::INITIAL_TX_INTERVAL_SEC;
}

} // namespace radio
