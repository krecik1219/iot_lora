#pragma once

#include "LoraCfg.hpp"
#include "PayloadCb.hpp"
#include "UplinkCb.hpp"

namespace env {
struct EnvRuntimeCfg;
} // namespace envs

namespace radio {

class LoraCtrl final
{
public:
    LoraCtrl(const LoraCtrl&) = delete;
    LoraCtrl& operator=(const LoraCtrl&) = delete;
    LoraCtrl(LoraCtrl&&) = delete;
    LoraCtrl& operator=(LoraCtrl&&) = delete;

    static LoraCtrl& initLoraCtrl(const LoraCfg& loraCfg, const env::EnvRuntimeCfg* const runtimeCfg);
    static LoraCtrl& getInstance();

    const LoraCfg& getCfg() const;
    osjob_t* getSendJobPtr();
    uint8_t* getPayload();

    void beginSending();
    void proceedScheduler();
    void payloadSet();
    void registerPayloadSetCb(PayloadCb payloadCb);
    void onUplinkReceived(const uint8_t* const data, const uint8_t dataLen);
    void registerUplinkCb(UplinkCb uplinkCb);

    uint32_t getTxInterval() const;

private:
    LoraCtrl();
    ~LoraCtrl();

    static LoraCtrl instance;
    static constexpr uint8_t payloadSize = 16;

    LoraCfg cfg;
    const env::EnvRuntimeCfg* runtimeCfg;
    uint8_t payload[payloadSize];
    osjob_t sendjob;
    PayloadCb payloadCb;
    UplinkCb uplinkCb;
};

} // namespace radio
