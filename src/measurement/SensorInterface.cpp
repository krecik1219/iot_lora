#include "SensorInterface.hpp"

namespace measurement {

SensorInterface::SensorInterface() = default;
SensorInterface::~SensorInterface() = default;
SensorInterface::SensorInterface(const SensorInterface &) = default;
SensorInterface &SensorInterface::operator=(const SensorInterface &) = default;
SensorInterface::SensorInterface(SensorInterface &&) = default;
SensorInterface &SensorInterface::operator=(SensorInterface &&) = default;

} // namespace measurement
