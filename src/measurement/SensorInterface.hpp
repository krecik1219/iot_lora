#pragma once

#include <Arduino.h>

namespace measurement {

enum class SensorType
{
    temp = 0,
    light
};

class SensorInterface
{
public:
    SensorInterface();
    virtual ~SensorInterface();
    SensorInterface(const SensorInterface&);
    SensorInterface& operator=(const SensorInterface&);
    SensorInterface(SensorInterface&&);
    SensorInterface& operator=(SensorInterface&&);

    virtual SensorType getType() const = 0;
    virtual float getReading() = 0;
};

} // namespace measurement
