#pragma once

#include <radio/PayloadCb.hpp>
#include <env/EnvConfiguration.hpp>
#include "SensorInterface.hpp"

namespace measurement {

class MeasurementCollector
{
public:
    MeasurementCollector();
    ~MeasurementCollector();

    MeasurementCollector(const MeasurementCollector&) = delete;
    MeasurementCollector& operator=(const MeasurementCollector&) = delete;
    MeasurementCollector(MeasurementCollector&&) = delete;
    MeasurementCollector& operator=(MeasurementCollector&&) = delete;

    void collectMeasurements();
    void addSensor(SensorInterface* sensor);
    const char* getData() const;

    radio::PayloadCb bindToCallback();

    static constexpr uint8_t dataSize = 16;

private:
    static void setPayload(uint8_t* payload, void* calleeContext);

    void resetData();
    void formatData(const float reading, const SensorType sensorType);
    void addCommaIfNotLast(const uint8_t sensorIndex);

    SensorInterface* sensors[env::SENSORS_NUM];
    uint8_t indexToInsert;
    char data[dataSize];
    uint8_t currentIndexToWrite;
};

} // namespace measurement
