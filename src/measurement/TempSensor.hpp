#pragma once

#include "SensorInterface.hpp"
#include <stdint.h>

namespace measurement {

class TempSensor : public SensorInterface
{
public:
    explicit TempSensor(const uint8_t analogPinNum);

    virtual SensorType getType() const override;
    virtual float getReading() override;

private:
    uint8_t analogPinNum;

    static constexpr int B = 4275;     // B value of the thermistor
    static constexpr int R0 = 100000;  // R0 = 100k
};

} // namespace measurement
