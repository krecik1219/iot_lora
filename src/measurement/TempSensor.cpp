#include "TempSensor.hpp"
#include <math.h>
#include <Arduino.h>

namespace measurement {

TempSensor::TempSensor(const uint8_t analogPinNum)
    : analogPinNum{analogPinNum}
{
}

SensorType TempSensor::getType() const
{
    return SensorType::temp;
}

float TempSensor::getReading()
{
    int a = analogRead(analogPinNum);

    float R = 1023.0 / a - 1.0;
    R = R0 * R;

    return 1.0 / (log(R / R0) / B + 1 / 298.15) - 273.15;
}

} // namespace measurement
