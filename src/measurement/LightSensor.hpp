#pragma once

#include "SensorInterface.hpp"
#include <stdint.h>

namespace measurement {

class LightSensor : public SensorInterface
{
public:
    explicit LightSensor(const uint8_t analogPinNum);

    virtual SensorType getType() const override;
    virtual float getReading() override;

private:
    uint8_t analogPinNum;
};

} // namespace measurement
