#include "DataFormatter.hpp"
#include <stdio.h>
#include <avr/pgmspace.h>
#include <stdlib.h>

namespace measurement {

uint8_t DataFormatter::formatReading(const float reading, const SensorType sensorType, char* buffer, const uint8_t posToInsert)
{
    switch(sensorType)
    {
        case SensorType::temp:
            return formatTempReading(reading, buffer, posToInsert);
        case SensorType::light:
            return formatLightReading(reading, buffer, posToInsert);
        default:
            break;
    }

    return posToInsert;
}

uint8_t DataFormatter::formatTempReading(const float reading, char* buffer, const uint8_t posToInsert)
{
    return posToInsert + snprintf_P(buffer + posToInsert, maxCharactersNum + 3, PSTR("T=%.1f"), reading);
}

uint8_t DataFormatter::formatLightReading(const float reading, char* buffer, const uint8_t posToInsert)
{
    return posToInsert + snprintf_P(buffer + posToInsert, maxCharactersNum + 3, PSTR("O=%.1f"), reading);
}


} // namespace measurement
