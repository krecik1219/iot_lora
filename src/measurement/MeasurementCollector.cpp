#include <Arduino.h>
#include "MeasurementCollector.hpp"
#include "DataFormatter.hpp"

namespace measurement {

MeasurementCollector::MeasurementCollector()
    : sensors{}
    , indexToInsert{0u}
    , data{"Hello World\0"}
    , currentIndexToWrite{0u}
{
}

MeasurementCollector::~MeasurementCollector() = default;

void MeasurementCollector::collectMeasurements()
{
    resetData();
    for(auto i = 0u; i < indexToInsert; i++)
    {
        auto* sensor = sensors[i];
        const float reading = sensor->getReading();
        const SensorType sensorType = sensor->getType();
        formatData(reading, sensorType);
        addCommaIfNotLast(i);
    }
}

void MeasurementCollector::addSensor(SensorInterface* sensor)
{
    if(indexToInsert >= env::SENSORS_NUM)
        return;

    sensors[indexToInsert] = sensor;
    indexToInsert++;
}

const char* MeasurementCollector::getData() const
{
    return data;
}

radio::PayloadCb MeasurementCollector::bindToCallback()
{
    return {this, &MeasurementCollector::setPayload};
}

void MeasurementCollector::setPayload(uint8_t* payload, void *calleeContext)
{
    auto* self = static_cast<MeasurementCollector*>(calleeContext);
    self->collectMeasurements();
    memcpy(payload, self->data, dataSize);
}

void MeasurementCollector::resetData()
{
    currentIndexToWrite = 0u;
    memset(data, 0, sizeof(data));
}

void MeasurementCollector::formatData(const float reading, const SensorType sensorType)
{
    currentIndexToWrite = DataFormatter::formatReading(reading, sensorType, data, currentIndexToWrite);
}

void MeasurementCollector::addCommaIfNotLast(const uint8_t sensorIndex)
{
    if(sensorIndex + 1 >= indexToInsert)
        return;

    data[currentIndexToWrite] = ',';
    currentIndexToWrite++;
}

} // namespace measurement
