#include "LightSensor.hpp"
#include <Arduino.h>

namespace measurement {

LightSensor::LightSensor(const uint8_t analogPinNum)
    : analogPinNum{analogPinNum}
{
}

SensorType LightSensor::getType() const
{
    return SensorType::light;
}

float LightSensor::getReading()
{
    return static_cast<float>(analogRead(analogPinNum));
}

} // namespace measurement
