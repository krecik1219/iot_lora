#pragma once

#include "SensorInterface.hpp"
#include <stdint.h>
#include <stddef.h>

namespace measurement {

class DataFormatter
{
public:
    static uint8_t formatReading(const float reading, const SensorType sensorType, char* buffer, const uint8_t posToInsert);

private:
    static uint8_t formatTempReading(const float reading, char* buffer, const uint8_t posToInsert);
    static uint8_t formatLightReading(const float reading, char* buffer, const uint8_t posToInsert);

    static constexpr size_t maxCharactersNum = 4;
};

} // namespace measurement
