# LoRa IoT node
This project uses Arduino CMake toolchain a submodule: [https://github.com/arduino-cmake/Arduino-CMake-NG](https://github.com/arduino-cmake/Arduino-CMake-NG)

1. Download arduino sdk: https://downloads.arduino.cc/arduino-1.8.13-linux64.tar.xz
2. Unpack arduino SDK to /usr/share dir
3. Downlad this repo: git clone --recurse-submodules git@gitlab.com:krecik1219/iot_lora.git
4. Provide TTN credentials for your project in iot_lora/src/env/EnvConfiguration.hpp, pay attention to endianess
5. Install cmake and make if missing
6. Execute bellow commands

#### Building
```Shell
# assuming pwd is project's root dir
mkdir ../build && cd ../build
cmake cmake ../iot_lora/ -DCMAKE_TOOLCHAIN_FILE=../iot_lora/Arduino-CMake-NG/cmake/Arduino-Toolchain.cmake
make -j8
```  

#### Deploying  

```Shell
# assuming arduino sdk version is 1.8.13 and target board is Arduino Uno
# if not path has to be adjusted as well mcu type (see -patmega328p) and programmator type (-carduino)
# possibly serial devica has to be adjusted (-P/dev/ttyACM0)
/usr/share/arduino-1.8.13/hardware/tools/avr/bin/avrdude -C/usr/share/arduino-1.8.13/hardware/tools/avr/etc/avrdude.conf -v -patmega328p -carduino -P/dev/ttyACM0 -b115200 -D -Uflash:w:./build/arduino_device.hex
```
